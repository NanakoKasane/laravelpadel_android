**Aplicación en Android para conectarse al API hecha en Laravel para la reserva de una pista de padel.**

<p>Lo primero, teniendo en cuenta las rutas creadas de la API y explicadas anteriormente, configuro <b><i>RETROFIT</i></b> para su llamada (en <i>ApiTokenService</i>) </p>
<ul>
<li><i><b>getReservas</b> </i>: Para obtener todas las reservas del usuario actual </li>
<li><b><i>createReserva</b></i>: Para añadir una reserva, pasándole en el <i>@Body</i> la reserva</li>
<li><i><b>updateReserva </b></i>: Para modificar una reserva, pasándole en el <i>@Body</i> la reserva y en el @Path el id</li>
<li><i><b>deleteReserva </b></i>: Para borrar una reserva, pasándole en el @Path el id de la reserva a borrar <b></b></li>
<li><i><b>getReservasFromId</b></i> : Para obtener las reservas de un usuario en concreto (pasándole el id del usuario).  En este caso me fue necesario añadir <i>@FormUrlEncoded</i> y un <i>@Field</i> 
con el dato a pasar (id) para su funcionamiento</li>
<li><i><b>getReservasFromName</b></i> : Para obtener las reservas de un usuario en concreto (pasándole el nombre del usuario).  En este caso también me fue necesario añadir <i>@FormUrlEncoded</i> y un <i>@Field</i> 
con el dato a pasar (name) para su funcionamiento</li>
</ul>

<br/><br/>
<p>He creado para acceder a las siguientes opciones un <i><b>Navigation Drawer</b></i>, que pulsando en el menú superior se abre y se cierra y permite elegir la opción deseada, redirigiendo a ellas al pulsarla</p>
<p>Las <b>opciones</b> son las siguientes: </p>
<ol>
<li><b><i>Ver mis reservas </i>: </b> Esta opción permite ver tus reservas, modificarlas, borrarlas, añadir una nueva y enviar un email de invitación a un compañero. </li>
<p>Si se hace click en añadir o editar (editar es con un click y añadir mediante el botón flotante), permite establecer el número de horas
que se quiere reservar y la fecha. Compruebo que ambos datos sean válidos, así como que no se hagan reservas en el pasado ni con más de 7 días de antelación (esto 
lo realizo mediante el <i>DatePickerDialog</i> y la clase Calendar, poniéndole una fecha mínima y máxima a poder elegir)</p>
<p>Además, para mandar un email de invitación a esa reserva se accede mediante una pulsación larga en la reserva. Y al aceptar se envía el email mediante laravel</p>
<li><b><i>Buscar reservas por ID o por nombre</i></b>: Permite introducir un ID o nombre (según la opción elegida) y muestra la lista de reservas de este usuario. Si no existe el usuario o no tiene reservas, lo indica mediante un Toast y con un mensaje en un TextView </li>
<br/><li><b><i>Cerrar sesión </i></b>: Cierra la sesión, la borra de las preferencias y además vuelve al Login. </li>
</ol>