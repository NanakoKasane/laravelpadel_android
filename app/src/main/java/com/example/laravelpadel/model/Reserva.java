package com.example.laravelpadel.model;

import java.io.Serializable;

public class Reserva implements Serializable {
    private int id;
    private int numhoras;
    private String fecha;
    private int user_id;

    public Reserva(){

    }

    public Reserva(int numhoras, String fecha) {
        this.numhoras = numhoras;
        this.fecha = fecha;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumhoras() {
        return numhoras;
    }

    public void setNumhoras(int numhoras) {
        this.numhoras = numhoras;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return user_id + "\n" + fecha;
    }
}
