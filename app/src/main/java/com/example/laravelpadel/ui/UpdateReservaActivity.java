package com.example.laravelpadel.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laravelpadel.R;
import com.example.laravelpadel.model.Reserva;
import com.example.laravelpadel.network.ApiTokenRestClient;
import com.example.laravelpadel.util.SharedPreferencesManager;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateReservaActivity extends AppCompatActivity implements Callback<Reserva> {
    public static final int OK = 1;

    @BindView(R.id.tvId) TextView id;
    @BindView(R.id.edHoras) EditText edHoras;
    @BindView(R.id.edFecha) EditText edFecha;

    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    Reserva reserva;

    @OnClick(R.id.edFecha) void picker(){
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                edFecha.setText(year + "-" + month + "-" + day);
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(new Date().getTime());

        final LocalDate date;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            date = LocalDate.now();
            final LocalDate datePlus7Days = date.plusDays(7);
            Date date2 = Date.from(datePlus7Days.atStartOfDay(ZoneId.systemDefault()).toInstant());
            dialog.getDatePicker().setMaxDate(date2.getTime());
        }

        dialog.show();
    }

    @OnClick(R.id.accept)
    public void clickAccept(View view){
        String horas, fecha;
        Reserva reserva;

        hideSoftKeyboard();
        horas = edHoras.getText().toString();
        fecha = edFecha.getText().toString();
        if (horas.isEmpty() || fecha.isEmpty())
            Toast.makeText(this, "Debe introducir una fecha y horas", Toast.LENGTH_SHORT).show();
        else {
            int horasInt = Integer.parseInt(horas);
            if (horasInt > 2 || horasInt < 1){
                Toast.makeText(this, "El número de horas no puede ser mayor a 2", Toast.LENGTH_SHORT).show();
            }
            else{
                reserva = new Reserva(horasInt, fecha);
                connection(reserva);
            }

        }
    }

    @OnClick(R.id.cancel)
    public void clickCancel(View view){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatereserva);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);

        // Cargo los datos
        Intent i = getIntent();
        reserva = (Reserva) i.getSerializableExtra("reserva");
        id.setText("" + reserva.getId());
        edHoras.setText("" + reserva.getNumhoras());
        edFecha.setText("" + reserva.getFecha());

    }

    private void connection(Reserva reserva) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<Reserva> call = ApiTokenRestClient.getInstance(preferences.getToken()).createReserva(reserva);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Reserva> call, Response<Reserva> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Reserva r = response.body();

            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", r.getId());
            bundle.putInt("horas", r.getNumhoras());
            bundle.putString("fecha", r.getFecha());

            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Añadida reserva");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Reserva> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

