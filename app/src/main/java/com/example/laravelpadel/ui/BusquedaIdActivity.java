package com.example.laravelpadel.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laravelpadel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusquedaIdActivity extends AppCompatActivity {
    @BindView(R.id.edId) EditText edId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscarid);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.btBuscarid) void buscar(){
        if (TextUtils.isEmpty(edId.getText())){
            Toast.makeText(this, "Debe introducir el ID del usuario", Toast.LENGTH_SHORT).show();

        }
        else{
            Intent intent = new Intent(this, ReservasListBuscadasActivity.class);
            intent.putExtra("id", edId.getText().toString());
            startActivity(intent);
        }
    }
}
