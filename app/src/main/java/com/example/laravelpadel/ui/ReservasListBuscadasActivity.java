package com.example.laravelpadel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laravelpadel.R;
import com.example.laravelpadel.adapter.ClickListener;
import com.example.laravelpadel.adapter.RecyclerTouchListener;
import com.example.laravelpadel.adapter.ReservasAdapter;
import com.example.laravelpadel.model.Reserva;
import com.example.laravelpadel.network.ApiTokenRestClient;
import com.example.laravelpadel.network.RestClient;
import com.example.laravelpadel.util.SharedPreferencesManager;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservasListBuscadasActivity extends AppCompatActivity {

    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    ReservasAdapter reservasAdapter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.textView9) TextView tvTitulo;
    @BindView(R.id.tverror) TextView tvError;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservaslistbuscadas);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
        //showMessage("panel: " + preferences.getToken());

        //Initialize RecyclerView
        reservasAdapter = new ReservasAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(reservasAdapter);


        if (getIntent().getStringExtra("id") != null){
            String id = getIntent().getStringExtra("id");
            tvTitulo.setText(tvTitulo.getText().toString() + "" + id);
//            int idInt = Integer.parseInt(id);
            downloadReservasId(id);

        }
        else if (getIntent().getStringExtra("name") != null){
            String name = (getIntent().getStringExtra("name"));
            tvTitulo.setText(tvTitulo.getText().toString() + "" + name);
            downloadReservasName(name);
        }


    }


    private void downloadReservasId(String id) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<ArrayList<Reserva>> call = ApiTokenRestClient.getInstance(preferences.getToken()).getReservasFromId(id);
        call.enqueue(new Callback<ArrayList<Reserva>>() {
            @Override
            public void onResponse(Call<ArrayList<Reserva>> call, Response<ArrayList<Reserva>> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
//                    adapter.setRepos(response.body());
                    reservasAdapter.setReservas(response.body());

                    showMessage("Reservas descargadas");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                    tvError.setText("El usuario no existe o no tiene reservas añadidas");

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Reserva>> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());
            }
        });
    }


    private void downloadReservasName(String name) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<ArrayList<Reserva>> call = ApiTokenRestClient.getInstance(preferences.getToken()).getReservasFromName(name);
        call.enqueue(new Callback<ArrayList<Reserva>>() {
            @Override
            public void onResponse(Call<ArrayList<Reserva>> call, Response<ArrayList<Reserva>> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
//                    adapter.setRepos(response.body());
                    reservasAdapter.setReservas(response.body());

                    showMessage("Reservas descargadas");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                        tvError.setText("El usuario no existe o no tiene reservas añadidas");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Reserva>> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());
            }
        });


    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }



}
