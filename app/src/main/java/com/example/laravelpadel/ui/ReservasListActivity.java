package com.example.laravelpadel.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.laravelpadel.R;
import com.example.laravelpadel.adapter.ClickListener;
import com.example.laravelpadel.adapter.RecyclerTouchListener;
import com.example.laravelpadel.adapter.ReservasAdapter;
import com.example.laravelpadel.model.Reserva;
import com.example.laravelpadel.network.ApiTokenRestClient;
import com.example.laravelpadel.util.SharedPreferencesManager;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservasListActivity extends AppCompatActivity {

    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;
    public static final String MAIL = "mail";

    @BindView(R.id.floatingActionButton) FloatingActionButton fab;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    int positionClicked;
    private ReservasAdapter reservasAdapter;

    ProgressDialog progreso;
    //ApiService apiService;
    SharedPreferencesManager preferences;

    @OnClick(R.id.floatingActionButton)
    public void Click(View view) {
        Intent i = new Intent(this, AddReservaActivity.class);
        startActivityForResult(i, ADD_CODE);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservas_list);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
        //showMessage("panel: " + preferences.getToken());

        //Initialize RecyclerView
        reservasAdapter = new ReservasAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(reservasAdapter);
        //manage click
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showPopup(view, position);
            }
            @Override
            public void onLongClick(View view, int position) {
                Intent emailIntent = new Intent(getApplicationContext(), EmailActivity.class);
                emailIntent.putExtra("fecha", reservasAdapter.getAt(position).getFecha());
                emailIntent.putExtra("numhoras", reservasAdapter.getAt(position).getNumhoras());
                startActivity(emailIntent);
            }
        }));

        ApiTokenRestClient.deleteInstance();

        downloadReservas();
    }



    private void downloadReservas() {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<ArrayList<Reserva>> call = ApiTokenRestClient.getInstance(preferences.getToken()).getReservas();
        call.enqueue(new Callback<ArrayList<Reserva>>() {
            @Override
            public void onResponse(Call<ArrayList<Reserva>> call, Response<ArrayList<Reserva>> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
//                    adapter.setRepos(response.body());
                    reservasAdapter.setReservas(response.body());

                    showMessage("Reservas descargadas");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Reserva>> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());
            }
        });


    }



    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Reserva reserva = new Reserva();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                reserva.setId(data.getIntExtra("id", 1));
                reserva.setNumhoras(data.getIntExtra("horas", 0));
                reserva.setFecha(data.getStringExtra("fecha"));
                reservasAdapter.add(reserva);
            }

        if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                reserva.setId(data.getIntExtra("id", 1));
                reserva.setNumhoras(data.getIntExtra("horas", 0));
                reserva.setFecha(data.getStringExtra("fecha"));
                reservasAdapter.modifyAt(reserva, positionClicked);
            }
    }

    private void showPopup(View v, final int position) {
        PopupMenu popup = new PopupMenu(this, v);
        // Inflate the menu from xml
        popup.getMenuInflater().inflate(R.menu.popup_change, popup.getMenu());
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify_site:
                        modify(reservasAdapter.getAt(position));
                        positionClicked = position;
                        return true;
                    case R.id.delete_site:
                        confirm(reservasAdapter.getAt(position).getId(), position);
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Show the menu
        popup.show();
    }

    private void modify(Reserva r) {
        Intent i = new Intent(this, UpdateReservaActivity.class);
        i.putExtra("reserva", r);
        startActivityForResult(i, UPDATE_CODE);
    }

    private void confirm(final int id, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Quieres borrar la reserva " + id + "?")
                .setTitle("Borrar")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(final int position) {
        //Call<ResponseBody> call = ApiRestClient.getInstance().deleteSite("Bearer " + preferences.getToken(), adapter.getId(position));
        Call<ResponseBody> call = ApiTokenRestClient.getInstance(preferences.getToken()).deleteReserva(reservasAdapter.getId(position));
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(false);
        progreso.show();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    reservasAdapter.removeAt(position);
                    showMessage("Reserva cancelada");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Error borrando la reserva: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());
            }
        });
    }




}
