package com.example.laravelpadel.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.laravelpadel.LoginActivity;
import com.example.laravelpadel.R;
import com.example.laravelpadel.util.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawerlayout) DrawerLayout drawerLayout;
    @BindView(R.id.navigationview) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        ButterKnife.bind(this);
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setTitle(R.string.app_name);
        drawerLayout.openDrawer(GravityCompat.START);

        getSupportFragmentManager().beginTransaction().add(R.id.mcontent, new FragmentInicial()).commit();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }

            private void selectDrawerItem(MenuItem item) {
                Intent intent = null;

                switch (item.getItemId()){
                    case R.id.resevas_searchname:
                        intent = new Intent(BaseActivity.this, BusquedaNameActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.resevas_searchid:
                        intent = new Intent(BaseActivity.this, BusquedaIdActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.mis_resevas_view:
                        intent = new Intent(BaseActivity.this, ReservasListActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.logout:
                        SharedPreferencesManager preferences = new SharedPreferencesManager(BaseActivity.this);
                        preferences.saveToken(null, null);
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });



        drawerLayout.setDrawerShadow(android.R.color.darker_gray, GravityCompat.START);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_navigationhome);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
        }
        return true;

//        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
