package com.example.laravelpadel.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.example.laravelpadel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusquedaNameActivity extends AppCompatActivity {
    @BindView(R.id.edNombre) EditText edNombre;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscarname);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.btBuscarName) void buscar(){
        if (TextUtils.isEmpty(edNombre.getText())){
            Toast.makeText(this, "Debe introducir el nombre del usuario", Toast.LENGTH_SHORT).show();

        }
        else{
            Intent intent = new Intent(this, ReservasListBuscadasActivity.class);
            intent.putExtra("name", edNombre.getText().toString());
            startActivity(intent);
        }
    }
}
