package com.example.laravelpadel.network;

import com.example.laravelpadel.model.Email;
import com.example.laravelpadel.model.LoginResponse;
import com.example.laravelpadel.model.RegisterResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

// TODO. Login, register y sendemail
public interface ApiService {
    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);

    //@POST("api/register")
    //Call<RegisterResponse>register(@Body User user);

    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password);
    //@POST("api/login")
    //Call<LoginResponse>login(@Body User user);

//    @POST("api/logout")
//    Call<LogoutResponse> logout(
//            @Header("Authorization") String token);
//
//    @GET("api/sites")
//    Call<ArrayList<Site>> getSites(
//            @Header("Authorization") String token);
//
//    //@GET("acceso/sites.json")
//    //Call<ArrayList<Site>> getLocalSites();
//
//    @POST("api/sites")
//    Call<Site> createSite(
//            @Header("Authorization") String token,
//            @Body Site site);
//
//    @PUT("api/sites/{id}")
//    Call<Site> updateSite(
//            @Header("Authorization") String token,
//            @Body Site site,
//            @Path("id") int id);
//
//    @DELETE("api/sites/{id}")
//    Call<ResponseBody> deleteSite(
//            @Header("Authorization") String token,
//            @Path("id") int id);

    @POST("api/email")
    Call<ResponseBody> sendEmail(@Body Email email);
}

