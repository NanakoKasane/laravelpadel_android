package com.example.laravelpadel.network;

import com.example.laravelpadel.model.Reserva;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

// TODO. Aqui todo menos login, logout y register
public interface ApiTokenService {

    // TODO. obtengo TODAS mis reservas
    @GET("api/reservas")
    Call<ArrayList<Reserva>> getReservas(
            //@Header("Authorization") String token
    );

    // TODO. añado una reserva (debo pasarle numhoras y fecha por POST)
    @POST("api/reservas")
    Call<Reserva> createReserva(
            //@Header("Authorization") String token,
            @Body Reserva reserva);


    // TODO. Modifico una reserva
    @PUT("api/reservas/{id}")
    Call<Reserva> updateReserva(
            //@Header("Authorization") String token,
            @Body Reserva reserva,
            @Path("id") int id);


    // TODO. Borro una reserva
    @DELETE("api/reservas/{id}")
    Call<ResponseBody> deleteReserva(
            //@Header("Authorization") String token,
            @Path("id") int id);


    // TODO. Obtengo una reserva
    @GET("api/reservas/{id}")
    Call<Reserva> getReserva(
            //@Header("Authorization") String token,
            @Path("id") int id);


    // TODO. Obtengo las reservas del user con id iduser    @Body String iduser
    @FormUrlEncoded
    @POST("api/reservas.index2")
    Call<ArrayList<Reserva>> getReservasFromId(
            @Field("iduser") String iduser);



    // TODO. Obtengo las reservas del user con nombre name
    @FormUrlEncoded
    @POST("api/reservas.index3")
    Call<ArrayList<Reserva>> getReservasFromName(
            @Field("name") String name);



//    @POST("api/email")
//    Call<ResponseBody> sendEmail(@Body Email email);
}

